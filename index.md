**gras**
*italique*
![alt text](rafale.jpg "titre")
# Sample Markdown

This is some basic, sample markdown.

## Second Heading

 * Unordered lists, and:
  1. One
  1. Two
  1. Three
 * More

> Blockquote

And **bold**, *italics*, and even *italics and later **bold***. Even ~~strikethrough~~. [A link](https://markdowntohtml.com) to somewhere.

And code highlighting:

```C#
class Bar
{
    string bar;
    string manger;

    public Bar(bar, manger)
        this.bar = bar;
        this.manger = manger;
    
}
```

Or inline code like `var foo = 'bar';`.

Or an image of bears

![bears](http://placebear.com/200/200)

The end ...
